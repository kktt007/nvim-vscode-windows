# vs nvim font configuration

## visual studio code windows fonts:

- "editor.fontFamily": "'MesloLGMDZ NF', 'FiraCode NF', 'Consolas', 'monospace', monospace, '楷体', '98WB-0'",
  "editor.fontLigatures": true,//这个控制是否启用字体连字，true启用，false不启用，这里选择启用

> OTF——opentype 苹果机与PC机都能很好应用的兼容字体！

> TTF——truetype PC机应用较好，苹果机兼容性很差！

C:\Users\Administrator\AppData\Roaming\Code\User 为windows配置文件路径

## nvim 

1. 安装git
2. 安装chocolatey 并安装make

3. 需要安装cygwin: https://mirrors.tuna.tsinghua.edu.cn镜像

一以插件编译错误  需要拷贝 vimproc_cygwin32.dll vimproc_cygwin64.dll 一定要改成这2个文件名
此处可下载源文件 https://github.com/Shougo/vimproc.vim/releases 改成以上文件名
\Users\Administrator\.cache\vimfiles\repos\github.com\Shougo\vimproc.vim\lib\

安装以下

```
binutils

gcc-core 
gcc-g++

mingw64-x86_64-gcc-core
mingw64-x86_64-gcc--fortran
mingw64-x86_64-gcc-g++

gdb
```

- 添加bin路径到环境

- 测试按装效果
  cygcheck -c cygwin
  然后依次输入gcc –version，g++ --version，make –version，gdb –version进行测试，如果都打印出版本信息和一些描述信息，非常高兴的告诉你，你的cygwin安装完成了